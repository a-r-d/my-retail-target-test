const mongoose = require('mongoose');

const ProductPricing = new mongoose.Schema({
  productId: { type: String, index: true, unique: true },
  price: Number,
  currencyCode: String,
  updated: { type: Date, default: Date.now },
});

module.exports = mongoose.model('ProductPricing', ProductPricing);
