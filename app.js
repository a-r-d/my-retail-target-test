const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('config');

// routes
const index = require('./routes/index');
const product = require('./routes/product');

// middleware
const errors = require('./middleware/errors');
const notfound = require('./middleware/not-found');

// Schemas
const ProductPricing = require('./models/product-pricing');

const app = express();

// middleware before routes
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// routes
app.use('/', index);
app.use('/products', product);

// middleware after routes
app.use(notfound);
app.use(errors);

mongoose.connect(config.mongo.uri, function(error){
  if(error){
    throw new Error("Could not connect to mongo!");
  }
  console.info("Connected to mongo successfully...");
});

module.exports = app;
