#!/usr/bin/env bash
docker run --net=host -p 27017:27017 --name mongodb -d mongo:3.5.6
sleep 5
docker run --net=host -e NODE_ENV='production' -e PORT='9999' -e NODE_CONFIG_STRICT_MODE='false' -p 9999:9999 --name myretail-server -d myretail-node-app
