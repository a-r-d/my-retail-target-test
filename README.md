# MyRetail RESTful service example #

This application provides both product details and pricing information for product Ids that are passed to it. This is a demo application for an interview process. There are some things I would have liked to add, such as a better logging system, and Swagger and Hystrix integrations.

## Architecture:

The application conceptually works as follows:

![Diagram](./docs/MyRetail-diagram.png)

The Data is retrieved from the underlying services in parallel, cached, and returned transparently to the consumer.

The consumer may also PUT pricing information for a given a product ID, and the datastore will the update the data, and refresh the cache.


## Application Infrastructure:

Each component of the application is dockerized and may be run from a Docker compose file. The components are listed as follows:

1. A Node Server with several instances clustered behind PM2 which is the "MyRetail" HTTP Service
2. A MongoDB instance seeded with pricing information
3. An LRU In-Memory cache that is part of the Node process.
4. A remote third-part service that returns information about products.


## API Documentation

### GET /products/:id

Returns a product in the following format:

```
{
  "name": "Conan the Barbarian (dvd_video)",
  "id": "13860427",
  "current_price": {
    "value": 501,
    "currency_code": "USD"
  }
}
```

Returns 404 if either the product is not found in the remote service OR the product is not found in the pricing.

### PUT /products/:id

Allows you to update pricing information for the product. You can pass the following fields in the message body. If you pass only one key ('e.g' "current_price.currency_code") only that one value will be updated.

```
{
  "current_price": {
    "value": 501,
    "currency_code": "USD"
  }
}
```

Returns the updated product pricing entity if successful, or a 404 if the pricing for the product was not found.


## Running Locally

To run the application locally you will need Node.js installed (version 6.x) and MongoDB. The steps to get the application working if these dependencies are met should be as simple as:

```
npm i
npm start
```

You will also want to add some fake pricing information to the database. You can do this by using the following NPM scripts (one for each environment):

```
npm run load-fixtures-dev
npm run load-fixtures-prod
```

The application runs on port 9999 in the npm scripts by default. You can test the application by hitting an endpoint like so:

```
curl http://localhost:9999/products/13860427
```

## Running Tests

The application has tests with Mocha and Supertest. The tests are integration tests and use database fixtures. Therefore, __a local instance of MongoDB must be accessible for the tests to run__. You can run the tests as follows:

```
npm test
```

This will also run the linter.


## Deployment

The application can be deployed via docker. The dockerized version of the application runs inside of PM2 node process manager with load balancing.

Any configuration parameter (such as sensitive things, like DB connection strings) can be overriden via environmental variable at the time you run the docker container. Check here for details on how the config library works:

https://github.com/lorenwest/node-config/wiki/Environment-Variables


## Deploying with Docker:

You can use the docker scripts to build and run the application via docker
```
npm run build-docker
npm run start-docker
```

These commands will start two docker containers - one is for the  MyRetail HTTP Server, the other starts a mongo instance from the official public mongo repo on Docker hub.

You may want to add the fixtures data to the server like so:

```
npm run load-fixtures-prod
```
