const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({ status: 'OK', name: 'MyRetail RESTful Product Service' });
});

module.exports = router;
