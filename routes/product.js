const express = require('express');
const async = require('async');
const ProductPricing = require('../models/product-pricing');
const _ = require('lodash');
const { getProductDetails } = require('../lib/product-catalog-client');
const { getProductCache, updateProductCache, evict } = require('../lib/product-cache');

const router = express.Router();

router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  const productCache = getProductCache(id);
  if (productCache) {
    return res.json(productCache);
  }

  let details;
  let pricing;
  return async.parallel([
    (callback) => {
      getProductDetails(id, (err, response, body) => {
        if (err) {
          return callback(err);
        }
        details = body;
        return callback();
      });
    }, (callback) => {
      ProductPricing.findOne({ productId: id }, (err, productPricing) => {
        if (err) {
          return callback(err);
        }
        if (!productPricing) {
          const notFound = new Error(`Product Pricing Not Found for id=${id}!`);
          notFound.status = 404;
          return callback(notFound);
        }
        pricing = productPricing;
        return callback();
      });
    }
  ], (error) => {
    if (error) {
      return next(error);
    }

    const productInformation = {
      name: _.get(details, 'product.item.product_description.title'),
      id,
      current_price: {
        value: _.get(pricing, 'price'),
        currency_code: _.get(pricing, 'currencyCode')
      }
    };
    updateProductCache(id, productInformation);
    return res.json(productInformation);
  });
});

router.put('/:id', (req, res, next) => {
  const id = req.params.id;
  evict(id);

  function parseAndSetValues(productPricing) {
    const inValue = _.get(req, 'body.current_price.value');
    if (_.isNumber(inValue)) {
      productPricing.price = inValue;
    }
    const inCode = _.get(req, 'body.current_price.currency_code');
    if (_.isString(inCode)) {
      productPricing.currencyCode = inCode;
    }
  }

  ProductPricing.findOne({ productId: id }, (err, productPricing) => {
    if (err) {
      return next(err);
    }
    if (!productPricing) {
      const notFound = new Error(`Product Pricing Not Found for id=${id}!`);
      notFound.status = 404;
      return next(notFound);
    }
    parseAndSetValues(productPricing);

    return productPricing.save((error) => {
      if (error) {
        return next(error);
      }
      return res.json(productPricing);
    });
  });
});

module.exports = router;
