module.exports = function errors(err, req, res, next) { // eslint-disable-line no-unused-vars
  // errors are logged in development, and returned to the user as well
  const detail = process.env.NODE_ENV === 'development' ? err.stack : {};
  res.status(err.status || 500);

  // TODO: env configurable logging.
  // always log full error.
  console.error(err);
  return res.json({
    message: err.message,
    error: detail
  });
};
