const should = require('should');
const request = require('supertest');
const server = require('../../app');
const _ = require('lodash');

describe('Routes Test: ', function() {

  before(function(done) {
      const dataloader = require('../helpers/dataloader');
      dataloader(done);
  });

  describe('products', function() {
    describe('GET /:id', function() {

      it('should return product information', function(done) {
        request(server)
          .get('/products/13860425')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);
            //console.log(res.body);
            const name = _.get(res, 'body.name');
            const currencyCode = _.get(res, 'body.current_price.currency_code');
            const currencyPrice = _.get(res, 'body.current_price.value');

            name.should.eql('Godzilla (Criterion Collection) (Blu-ray)');
            currencyCode.should.eql('USD');
            res.body.id.should.eql('13860425');
            done();
          });
      });

      it('should return product information cached (should be very fast)', function(done) {
        const start = new Date();
        request(server)
          .get('/products/13860425')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);
            //console.log(res.body);
            const name = _.get(res, 'body.name');
            const currencyCode = _.get(res, 'body.current_price.currency_code');
            const currencyPrice = _.get(res, 'body.current_price.value');

            name.should.eql('Godzilla (Criterion Collection) (Blu-ray)');
            currencyCode.should.eql('USD');
            res.body.id.should.eql('13860425');
            const end = new Date();

            const duration = end.getTime() - start.getTime();
            console.log('Duration: ', duration);
            duration.should.be.below(10); // less than 10 ms, ensure cache is working
            done();
          });
      });

    });

    describe('GET /:id (invalid ID)', function() {

      it('should return a 404 since the record is not found in DB', function(done) {
        request(server)
          .get('/products/23860425')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(404)
          .end(function(err, res) {
            should.not.exist(err);
            //console.log(res.body);
            res.body.message.should.eql('Product Pricing Not Found for id=23860425!');
            done();
          });
      });

    });

    describe('PUT /:id', function() {
      it('should start with a currency code of USD', function(done) {
        request(server)
          .get('/products/13860428')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);
            const name = _.get(res, 'body.name');
            const currencyCode = _.get(res, 'body.current_price.currency_code');
            currencyCode.should.eql('USD');
            done();
          });
      });

      it('should update product pricing information and currency code', function(done) {
        const updates = {
          "current_price": {
            "value": 501.11,
            "currency_code": "CAD"
          }
        };
        request(server)
          .put('/products/13860428')
          .send(updates)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);
            const name = _.get(res, 'body.name');
            const currencyCode = _.get(res, 'body.currencyCode');
            const currencyPrice = _.get(res, 'body.price');

            currencyCode.should.eql('CAD');
            currencyPrice.should.eql(501.11);
            done();
          });
      });

      it('should get a 404 when we try to update an uknown price', function(done) {
        const updates = {
          "current_price": {
            "value": 501.11,
            "currency_code": "CAD"
          }
        };
        request(server)
          .put('/products/7860428')
          .send(updates)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(404)
          .end(function(err, res) {
            should.not.exist(err);
            //console.log(res.body);
            res.body.message.should.eql('Product Pricing Not Found for id=7860428!');
            done();
          });
      });
    });

  });
});
