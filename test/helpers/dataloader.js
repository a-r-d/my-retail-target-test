if (process.env.NODE_ENV !== 'test') {
  throw new Error('Tests must use TEST env, data will be erased.');
}
const config = require('config');
const mongoose = require('mongoose');
const ProductPricing = require('../../models/product-pricing');
const dataFixtureInserter = require('../db-fixtures/insert-random-prices');
require('colors');

function eraseAndLoadData(callback) {
  console.info('Removing old productPricing data'.yellow);
  if(!mongoose.connection.readyState) {
    return mongoose.connect(config.mongo.uri, function(error){
      if(error){
        throw new Error("Could not connect to mongo!");
      }

      return ProductPricing.remove({}, function(err) {
        console.log('ProductPricing data cleared'.yellow);
        return dataFixtureInserter(callback);
      });
    });
  }
  
  return ProductPricing.remove({}, function(err) {
    console.log('ProductPricing data cleared'.yellow);
    return dataFixtureInserter(callback);
  });
}

module.exports = eraseAndLoadData;
