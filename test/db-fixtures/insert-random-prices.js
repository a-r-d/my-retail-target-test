const mongoose = require('mongoose');
const productList = require('./product-list');
const config = require('config');
const ProductPricing = require('../../models/product-pricing');
const async = require('async');
require('colors');

function insertEachProduct(cb) {
  console.info('Running data fixtures loader'.green);
  async.eachLimit(productList, 2, (productId, callback) => {
    console.info('Inserting....', productId);

    ProductPricing.create({
      productId,
      price: (Math.floor(Math.random() * 10000) + 1 )/ 100.00,
      currencyCode: 'USD'
    }, (err, productPricing) => {
      if(err) {
        console.error(err);
      }
      return callback();
    });
  }, (err) => {
    console.info('Done');
    return cb();
  });
}

function loadData(cb) {
  if(!mongoose.connection.readyState) {
    mongoose.connect(config.mongo.uri, function(error){
      if(error){
        throw new Error("Could not connect to mongo!");
      }
      console.info("Connected to mongo successfully...");
      insertEachProduct(cb);
    });
  } else {
    insertEachProduct(cb);
  }
}

if (require.main === module) {
  console.log('Running dataloader directly');
  loadData(() => {
    console.info('Script was called directly, exiting');
    process.exit();
  });
}

module.exports = loadData;
