const config = require('config');
const request = require('request');
const _ = require('lodash');

// TODO: this would be a good place to add environment based logging calls

function buildAPICall(productId) {
  return `${config.productServer.host}/v2/pdp/tcin/${productId}${config.productServer.args || ''}`;
}

function getProductDetails(productId, callback) {
  const uri = buildAPICall(productId);

  const options = {
    method: 'GET',
    url: uri,
    headers: {
      'cache-control': 'no-cache',
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'
    }
  };
  console.info('Hitting upstream API: ', uri);

  request(options, (error, response, body) => {
    const code = _.get(response, 'statusCode');
    // parse OK response
    if (body && code === 200) {
      return callback(error, response, JSON.parse(body));
    }
    // explicitely handle this common scenario
    if (code === 404) {
      const notFound = new Error(`Product information not found for id=${productId}`);
      notFound.status = 404;
      return callback(notFound, response);
    }
    // this is an error state probably, add the code to the error status.
    if (error && code) {
      error.status = code;
    }
    return callback(error, response, body);
  });
}

module.exports = { getProductDetails };
